﻿using System;

// Abstract class defining the template method
abstract class FruitTemplate
{
    public void PrepareFruit()
    {
        WashFruit();
        PeelFruit();
        SliceFruit();
        ServeFruit();
    }

    protected abstract void WashFruit();
    protected abstract void PeelFruit();
    protected abstract void SliceFruit();

    protected virtual void ServeFruit()
    {
        Console.WriteLine("Serve the fruit.");
    }
}

// Concrete subclass for preparing an apple
class Apple : FruitTemplate
{
    protected override void WashFruit()
    {
        Console.WriteLine("Wash the apple.");
    }

    protected override void PeelFruit()
    {
        Console.WriteLine("Peel the apple.");
    }

    protected override void SliceFruit()
    {
        Console.WriteLine("Slice the apple.");
    }
}

// Concrete subclass for preparing a banana
class Banana : FruitTemplate
{
    protected override void WashFruit()
    {
        Console.WriteLine("Wash the banana.");
    }

    protected override void PeelFruit()
    {
        Console.WriteLine("Peel the banana.");
    }

    protected override void SliceFruit()
    {
        Console.WriteLine("Slice the banana.");
    }
}

class Program
{
    static void Main()
    {
        Console.WriteLine("Preparing an Apple:");
        FruitTemplate apple = new Apple();
        apple.PrepareFruit();

        Console.WriteLine("\nPreparing a Banana:");
        FruitTemplate banana = new Banana();
        banana.PrepareFruit();
    }
}
